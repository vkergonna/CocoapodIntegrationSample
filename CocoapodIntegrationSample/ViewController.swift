//
//  ViewController.swift
//  SwiftPackageIntegrationSample
//
//  Created by Vincent Kergonna on 15/11/2022.
//

import UIKit
import SwiftUI
import MiamIOS

@available(iOS 14, *)
class ViewController: UIHostingController<CatalogView> {
    // Initialize our controller with RecipeCardView as a root view and show
    // recipe 1.
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, rootView: CatalogView())
    }
}

